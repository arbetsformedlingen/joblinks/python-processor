# python-processor

Datahandling (input file or stdin, output stdout)  
Chaos test  
For reuse in other processors

The code must be in a folder with the same name as the repo.
In this case: 
Repo: `python-processor`
pyproject.toml: `packages = [{include = "python_processor"}]`

To use this library when artefacts have been published to JobTech PyPi:
Add this section to the `pyproject.toml`in your project:
```
[[tool.poetry.source]]
name = "jobTech"
url = "https://pypi.jobtechdev.se/repository/pypi-jobtech/simple/"
default = false
secondary = true
```
The libraries imported into this project (e.g. *Loguru* will be available in your project as well)

After installation (`poetry install`) you should be able to import both the `python_processor`and `loguru` and use the python-processor like this:


```python
from loguru import logger
from python_processor.data_handler import DataHandler
from python_processor.chaos import ChaosTest
import my_processor  # the code for processing ads

if __name__ == '__main__':
    with DataHandler() as dh, ChaosTest():
        # DataHandler will read stdin / file at start and write to stdout when leaving the scope. 
        # This is configured by environment variables.
        dh.ads = my_processor.process_all_ads(dh.ads)
        logger.info(f"Converted {len(dh.ads)} ads.")

        
    pass # your code
```


Environment variables
`USE_STDIN`, default value False
`FILE_NAME`, default value 'output.json', used if USE_STDIN is False
`LOGURU_LEVEL`, default value 'INFO' for logging library "Loguru"
`PRINT`, default value True. Determines if ads should be written to stdout at completion.
