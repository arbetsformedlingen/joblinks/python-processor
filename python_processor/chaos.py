import sys
import random
from loguru import logger


class ChaosTest():
    """
        if command line arg --chaos-test is used, there should be 5% risk of exit
        this test is executed automatically when this class is used with a 'with' statement
        e.g 'with ChaosTest:'
    """

    def __init__(self):
        self.run_chaos_test = len(sys.argv) > 1 and sys.argv[1] == '--chaos-test'
        self.error = self._potential_chaos()

    def _potential_chaos(self):
        if self.run_chaos_test:
            logger.info('chaos test started')
            return random.randint(1, 20) == 1
        else:
            return False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if self.error and self.run_chaos_test:
            logger.error("chaos test failed")
            sys.exit(1)
        elif self.run_chaos_test:
            logger.info("chaos test passed")
            return True
