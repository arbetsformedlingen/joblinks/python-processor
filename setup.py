from setuptools import setup, find_packages

setup(
    name='PythonProcessor',
    version='0.1.0',
    url='https://gitlab.com/joblinks/python-processor',
    description='Base for creating a python processor, handles input/output and chaos-test',
    packages=find_packages(),
    install_requires=['ndjson']
)
