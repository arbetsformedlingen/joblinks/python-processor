import os
from distutils.util import strtobool

USE_STDIN = strtobool(os.getenv("USE_STDIN", default='true'))
FILE_NAME = os.environ.get("FILE_NAME", default='output.json')
LOGURU_LEVEL = os.environ.get("LOG_LEVEL", default="INFO")
PRINT = strtobool(os.getenv("PRINT", default='true'))
